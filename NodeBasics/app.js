const express = require("express");
const app = express();
const bodyparser = require("body-parser")
const productRoute = require("./Routes/productRouter")
const shopRoute = require("./Routes/shopRouter")
const path = require("path");
app.use(bodyparser.urlencoded({extended:false}))
app.use("/admin",productRoute)
app.use(shopRoute)


app.use((req,res,next)=>{
    res.status(404).sendFile(path.join(__dirname,"views","error.html"))
})

app.listen(3002,()=>{
    console.log("Server is Ready")
})
